## RELEASE HISTORY

##### _Release date: 2021-12-14_

| Name   | Description |
| ------ | ------ |
| Application Name | atrfiddemo-release-V2.01.2021121400.beta.apk |
| Demo Version | V2.01.2021121400.beta |
| Supported F/W Version | v4.9.6.1 and above |

## What's New

- [ ] 6B and Rail Tag Inventory Feature added
   -  *by default 6C tag inventory is set.*

 <details>
     <summary markdown="span" >Screenshots</summary>
      <div align="left">
        <img width="24%" src="https://gitlab.com/atid-share/asset/-/raw/main/sample_00.png"  alt="Setting screen" title="Settings"</img>
        <b> >> </b>
        <img width="24%" src="https://gitlab.com/atid-share/asset/-/raw/main/sample_01.png" alt="List screen" title="Setting Lists"></img>
        <b> >> </b>
        <img width="24%" src="https://gitlab.com/atid-share/asset/-/raw/main/sample_02.png" alt="dialog " title="Option dialog"></img>
       </div>
      </details>


## Support
 For SDK and other inquiries please contact inquiry@atid1.com
